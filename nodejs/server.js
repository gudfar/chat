var config = require('./config');
var express = require('express');
var crypto = require('crypto');


var app = express();
var server;
var mysql = require('mysql');
var users = {};

var connection;

function mysqlConnect() {

    connection = mysql.createConnection({
        host     : config.mysql.host,
        user     : config.mysql.user,
        password : config.mysql.password,
        database : config.mysql.database
    });

    connection.connect(function(err) {
        if(err) {
            console.log('error when connecting to mysql:', err);
            setTimeout(mysqlConnect, 2000);
        }
    });

    connection.on('error', function(err) {
        console.log('mysql error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') {
            mysqlConnect();
        } else {
            throw err;
        }
    });
}
//connect to mysql
mysqlConnect();


//choose server https/http
if (config.httpsEnable) {
  var https = require('https');
  var fs = require('fs');
  options = {
    key: fs.readFileSync(config.httpsServer.certificateKey),
    cert: fs.readFileSync(config.httpsServer.certificate)
  };
  server = https.createServer(options, app);
} else {
  var http = require('http');
  server = http.createServer(app);
}


server.listen(config.port);
var io = require('socket.io')(server);

var chatNsp = io.of('/chat');

//On connect
chatNsp.use(function(socket, next){
    var userId = socket.handshake.query.userId;
    if (userId !== undefined) {
        users[userId] = socket;
    }
    return next();
});



chatNsp.on('connection', function (socket) {
    socket.on('pushMessage', function (data) {
        connection.query('INSERT INTO messages VALUES(null, ?, ?, ?, NOW(), NOW())', [data.from_user_id, data.to_user_id, data.text], function (error, results) {
            if (error) {
                console.log(error);
            }
        });

        var clientSocket = users[data.to_user_id];

        if (clientSocket !== undefined) {
            clientSocket.emit('sendMessage', data);
        }
    });

    socket.on('addCaption', function (data) {
        var clientSocket = users[data.to_user_id];
        if (clientSocket !== undefined) {
            clientSocket.emit('addCaption', {username: data.from_username});
        }
    });

    socket.on('removeCaption', function (data) {
        var clientSocket = users[data.to_user_id];
        if (clientSocket !== undefined) {
            clientSocket.emit('removeCaption');
        }
    });
});