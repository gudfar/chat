<?php

namespace App;

use App\Message;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $profile_image
 * @property string $profile_image_path
 * @property integer $timezone_id
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereProfileImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereProfileImagePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'timezone_id'
    ];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = self::generateId();
        });
    }

    /**
     * @return string
     */
    public static function generateId (): string
    {
        while (true) {
            $id = uniqid();
            if (!self::find($id)) {
                return $id;
            }
        }
    }

    /**
     * @return array
     */
    public function editProfileRules(): array
    {
        return [
            'profile_image' => ['sometimes', 'image', 'mimes:jpg,png,jpeg', 'max:2048'],
            'timezone_id' => ['required', 'integer'],
            'name' => ['required', Rule::unique('users')->ignore($this->id), 'max:255'],
            'email' => ['required', Rule::unique('users')->ignore($this->id), 'email', 'max:255'],
        ];
    }

    /**
     * @return string
     */
    public function getProfileImage(): string
    {
        if (!empty($this->profile_image_path) || !empty($this->profile_image)) {
            return url($this->profile_image_path . $this->profile_image);
        }
        return url('/uploads/user/profile/user.png');
    }



    /**
     * 1. sometimes and min не работает
     * 2. валидация текущего пароля
     *
     * @return array
     */
    public function changePasswordRules(): array
    {
        return [
            'current_password' => 'required|checkCurrentPassword:'. $this->password,
            'new_password' => 'required|min:6|max:255',
            'repeat_password' => 'required|same:new_password|max:255',
        ];
    }

    /**
     * @param string $key
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messagesByKey(string $key)
    {
        return $this->hasMany(Message::class, $key);
    }

    /**
     * @param array $data
     * @param array $rules
     * @return \Illuminate\Validation\Validator
     */
    public function validate(array $data, array $rules): \Illuminate\Validation\Validator
    {
        return Validator::make($data, $rules);
    }

}
