<?php

namespace App\Validators;

use Hash;
use Illuminate\Validation\Validator;

/**
 * Class CurrentPasswordValidator
 * @package App\Validators
 */
class CurrentPasswordValidator
{

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param Validator $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator): bool
    {
        return Hash::check($value, $parameters[0]);
    }


}