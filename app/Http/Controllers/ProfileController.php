<?php

namespace App\Http\Controllers;

use App\Helpers\FileHelper;
use App\User;
use JsValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;

class ProfileController extends Controller
{

    /**
     * ProfileController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {

        $currentUser = Auth::user();
        if ($request->isMethod('post')) {
            $data = $request->all();

            $currentUser->validate($data, $currentUser->editProfileRules())->validate();
            if ($file = $request->file('profile_image')) {
                $fileName = md5(time()) . "." . $file->extension();
                $filePath = '/uploads/user/profile' . FileHelper::generateDynamicPath(Auth::id());
                $file->move(public_path($filePath), $fileName);

                $currentUser->profile_image = $fileName;
                $currentUser->profile_image_path = $filePath;
            }

            $currentUser->fill($data)->save();
        }

        return view('profile.edit', [
            'user' => $currentUser,
            'validator' => JsValidator::make($currentUser->editProfileRules())
        ]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function changePassword(Request $request)
    {
        $currentUser = Auth::user();
        if ($request->isMethod('post')) {
            $data = $request->all();
            $currentUser->validate($data, $currentUser->changePasswordRules())->validate();
            $currentUser->fill(['password' => Hash::make($data['new_password'])]);
            $currentUser->save();
        }
        return view('profile.change-password', [
            'user' => $currentUser,
            'validator' => JsValidator::make($currentUser->changePasswordRules())
        ]);
    }
}
