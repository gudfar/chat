<?php

namespace App\Http\Controllers;

use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    /**
     * ChatController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $id)
    {
        $users = User::where('id', '!=', Auth::id())->get();
        $messageScope = Message::messagesScope(Auth::id(), $id);
        return view('chat.index', [
            'users'        => $users,
            'messages'     => $messageScope,
            'countMessages' => count($messageScope),
        ]);
    }
}
