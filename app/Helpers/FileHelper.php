<?php

namespace App\Helpers;

/**
 * Class FileHelper
 * @package App\Helpers
 */
class FileHelper
{
    /**
     * @param string $id
     * @return string
     */
    public static function generateDynamicPath(string $id): string
    {
        $hash = md5((md5(time()) . (md5($id))));
        $firstLevel = substr($hash, 5, 4);
        $secondLevel = substr($hash, 10, 4);
        $thirdLevel = substr($hash, 20, 4);
        return DIRECTORY_SEPARATOR .$firstLevel. DIRECTORY_SEPARATOR.$secondLevel
            .DIRECTORY_SEPARATOR . $thirdLevel
            .DIRECTORY_SEPARATOR;
    }
}