<?php

namespace App\Helpers;

use Auth, DateTimeZone;
use Carbon\Carbon;

/**
 * Class DateHelper
 * @package App\Helpers
 */
class DateHelper
{

    public static function getFormatDate($date, string $format)
    {
        $timezone = self::getTimezoneByCode(Auth::user()->timezone_id);
        return Carbon::parse($date, 'UTC')->setTimezone($timezone)->format($format);
    }

    /**
     * @return array
     */
    public static function getTimezones(): array
    {
        return DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
    }

    /**
     * @param int|null $code
     * @return string
     */
    public static function getTimezoneByCode(?int $code): string
    {
        return DateTimeZone::listIdentifiers()[$code] ?? 'UTC';
    }

}