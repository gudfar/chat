<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function fromUser()
    {
        return $this->hasOne(User::class, 'id', 'from_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function toUser()
    {
        return $this->hasOne(User::class, 'id', 'to_user_id');
    }


    /**
     * @param string $id
     * @param string $opponentId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function messagesScope(string $id, string $opponentId)
    {
        $messages = Message::with('fromUser', 'toUser')
            ->where(function($query) use ($id, $opponentId) {
                $query->where('from_user_id', $id)
                    ->orWhere('from_user_id', $opponentId);
            })->where(function($query) use ($id, $opponentId) {
                $query->where('to_user_id', $id)
                    ->orWhere('to_user_id', $opponentId);
            })->orderBy('created_at', 'asc')->get();

        return $messages;
    }


    /**
     * @param Message|null $prevMessage
     * @param Message $currentMessage
     * @return null|string
     */
    public static function getDateSeparator(?Message $prevMessage, Message $currentMessage): ?string
    {

        $currentMessageDate = Carbon::parse($currentMessage->created_at, 'UTC')
            ->setTimezone(config('app.timezone'));

        if ($prevMessage === null) {
            if ($currentMessageDate->isYesterday()) {
                return "Yesterday";
            } elseif ($currentMessageDate->isToday()) {
                return "Today";
            } else {
                return $currentMessageDate->format('d F Y');
            }
        }

        $prevMessageDate = Carbon::parse($prevMessage->created_at, 'UTC')
            ->setTimezone(config('app.timezone'));

        if ($prevMessageDate->day !== $currentMessageDate->day) {
            if ($currentMessageDate->isYesterday()) {
                return "Yesterday";
            } elseif ($currentMessageDate->isToday()) {
                return "Today";
            } else {
                return $currentMessageDate->format('d F Y');
            }
        }

        return null;
    }
}
