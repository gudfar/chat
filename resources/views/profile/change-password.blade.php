@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="element-wrapper">
                <div class="element-box">


                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::open(['method' => 'post', 'route' => ['profile.change-password']]) !!}
                    <h5 class="form-header">Change password</h5>
                    <div class="form-group row">
                        <label class="col-form-label col-sm-4" for="">Current password</label>
                        <div class="col-sm-8">
                            {{ Form::password('current_password', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-sm-4" for="">New password</label>
                        <div class="col-sm-8">
                            {{ Form::password('new_password', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-sm-4" for="">Repeat password</label>
                        <div class="col-sm-8">
                            {{ Form::password('repeat_password', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-buttons-w">
                        {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
@endsection

@section('endBody')
    {!! $validator !!}
@endsection
