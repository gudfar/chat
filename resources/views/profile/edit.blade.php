@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="element-wrapper">
                <div class="element-box">


                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::open(['method' => 'post', 'route' => ['profile.edit'], 'files' => true]) !!}
                    <h5 class="form-header">Profile Edit</h5>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label" for="">Username</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">@</div>
                                    {{ Form::text('name', $user->name, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-sm-4" for="">Email</label>
                            <div class="col-sm-8">
                                {{ Form::text('email', $user->email, ['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-sm-4" for="">Timezone</label>
                            <div class="col-sm-8">
                                {{ Form::select('timezone_id', \App\Helpers\DateHelper::getTimezones(), $user->timezone_id, ['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-sm-4" for="">Profile image</label>
                            <div class="col-sm-8">
                                <img width="100%;" alt="" src="{{ $user->getProfileImage() }}"><br><br>
                                {{ Form::file('profile_image', ['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-buttons-w">
                            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
                        </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>

@endsection

@section('endBody')
    {!! $validator !!}
@endsection
