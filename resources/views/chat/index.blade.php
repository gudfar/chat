@extends('layouts.app')

@section('content')
                <div class="full-chat-w">
                    <div class="full-chat-i">
                        <div class="full-chat-left">
                            <div class="os-tabs-w">
                                <ul class="nav nav-tabs upper centered">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab_overview"><i class="os-icon os-icon-mail-14"></i><span>Chats</span></a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_sales"><i class="os-icon os-icon-ui-93"></i><span>Contacts</span></a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_sales"><i class="os-icon os-icon-ui-02"></i><span>Favorites</span></a></li>
                                </ul>
                            </div>
                            <div class="chat-search">
                                <div class="element-search">
                                    <input placeholder="Search users by name..." type="text">
                                </div>
                            </div>
                            <div class="user-list">
                                <!-- max 6 users -->
                                @foreach ($users as $user)
                                    <div class="user-w">
                                        <div class="avatar with-status status-green">
                                            <img alt="" src="{{ $user->getProfileImage() }}">
                                        </div>
                                        <div class="user-info">
                                            <div class="user-date">
                                                2 hours
                                            </div>
                                            <div class="user-name">
                                                {{ $user->name }}
                                            </div>
                                            <div class="last-message">
                                                Yes, I've sent you details...
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="full-chat-middle">
                            <div class="chat-head">
                                <div class="user-info">
                                    <span>To:</span>
                                    <a href="#">John Mayers</a>
                                </div>
                                <div class="user-actions">
                                    <a href="#"><i class="os-icon os-icon-mail-07"></i></a>
                                    <a href="#"><i class="os-icon os-icon-phone-18"></i></a>
                                    <a href="#"><i class="os-icon os-icon-phone-15"></i></a>
                                </div>
                            </div>
                            <div class="chat-content-w">
                                <div class="chat-content">

                                    @foreach ($messages as $key => $message)

                                        @if(\App\Message::getDateSeparator($messages[$key-1] ?? null, $message))
                                            <div class="chat-date-separator">
                                                <span>{{ \App\Message::getDateSeparator($messages[$key-1] ?? null, $message) }}</span>
                                            </div>
                                        @endif

                                        @if ($message->from_user_id === Auth::id())
                                            <div class="js-chat-message-block chat-message self" data-message-date="{{ \App\Helpers\DateHelper::getFormatDate($message->created_at, 'Y-m-d H:i:s') }}">
                                                <div class="chat-message-content-w">
                                                    <div class="chat-message-content">
                                                        {{ $message->text }}
                                                    </div>
                                                </div>
                                                <div class="chat-message-date">
                                                    {{ \App\Helpers\DateHelper::getFormatDate($message->created_at, 'H:i') }}
                                                </div>
                                                <div class="chat-message-avatar">
                                                    <img alt="" src="{{ $message->fromUser->getProfileImage() }}">
                                                </div>
                                            </div>
                                        @else
                                            <div class="js-chat-message-block chat-message" data-message-date="{{ \App\Helpers\DateHelper::getFormatDate($message->created_at, 'Y-m-d H:i:s') }}">
                                                <div class="chat-message-content-w">
                                                    <div class="chat-message-content">
                                                        {{ $message->text }}
                                                    </div>
                                                </div>
                                                <div class="chat-message-avatar">
                                                    <img alt="" src="{{ $message->fromUser->getProfileImage() }}">
                                                </div>
                                                <div class="chat-message-date">
                                                    {{ \App\Helpers\DateHelper::getFormatDate($message->created_at, 'H:i') }}
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                                <div id="caption-id" style="margin-left: 23px; color: rgba(0, 0, 0, 0.3);"></div><br><br>
                            </div>
                            <div class="chat-controls">
                                <div class="chat-input">
                                    <input placeholder="Type your message here..." type="text">
                                </div>
                                <div class="chat-input-extra">
                                    <div class="chat-extra-actions">
                                        <a href="#"><i class="os-icon os-icon-mail-07"></i></a>
                                        <a href="#"><i class="os-icon os-icon-phone-18"></i></a>
                                        <a href="#"><i class="os-icon os-icon-phone-15"></i></a>
                                    </div>
                                    <div class="chat-btn">
                                        <a class="btn btn-primary btn-sm" href="#">Reply</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="full-chat-right" style="display: none;">
                            <div class="user-intro">
                                <div class="avatar">
                                    <img alt="" src="img/avatar1.jpg">
                                </div>
                                <div class="user-intro-info">
                                    <h5 class="user-name">John Mayers</h5>
                                    <div class="user-sub">
                                        San Francisco, CA
                                    </div>
                                    <div class="user-social">
                                        <a href="#"><i class="os-icon os-icon-twitter"></i></a>
                                        <a href="#"><i class="os-icon os-icon-facebook"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="chat-info-section">
                                <div class="ci-header">
                                    <i class="os-icon os-icon-documents-03"></i>
                                    <span>Shared Files</span>
                                </div>
                                <div class="ci-content">
                                    <div class="ci-file-list">
                                        <ul>
                                            <li><a href="#">Annual Revenue.pdf</a></li>
                                            <li><a href="#">Expenses.xls</a></li>
                                            <li><a href="#">Business Plan.doc</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="chat-info-section">
                                <div class="ci-header">
                                    <i class="os-icon os-icon-documents-07"></i>
                                    <span>Shared Photos</span>
                                </div>
                                <div class="ci-content">
                                    <div class="ci-photos-list">
                                        <img alt="" src="img/portfolio9.jpg">
                                        <img alt="" src="img/portfolio2.jpg">
                                        <img alt="" src="img/portfolio12.jpg">
                                        <img alt="" src="img/portfolio14.jpg">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    <div class="js-chat-info" style="display: none;"
         data-socket-url="{{ config('app.webSocketUrl') }}"
         data-from-username="{{ \Illuminate\Support\Facades\Auth::user()->name }}"
         data-from-image="{{ \Illuminate\Support\Facades\Auth::user()->getProfileImage() }}"
         data-from-user-id="{{ \Illuminate\Support\Facades\Auth::id() }}"
         data-to-user-id="{{ Request::route('id') }}"
    ></div>
@endsection
