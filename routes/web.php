<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function ()    {
        //берем юзера с которым текущий юзер последний раз общался
        $user = \App\User::where('id', '!=', Auth::id())->first();
        return redirect()->route('chat.index', ['to_id' => $user->id]);
    });

    Route::get('/{id}', 'ChatController@index')->name('chat.index');
    Route::match(['get', 'post'], '/profile/edit', 'ProfileController@edit')->name('profile.edit');
    Route::match(['get', 'post'], '/profile/change-password', 'ProfileController@changePassword')->name('profile.change-password');

});



